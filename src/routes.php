<?php

use Illuminate\Support\Facades\Route;

Route::get('index', '\Seni\XlsPdf\DemoController@index');
Route::post('upload', '\Seni\XlsPdf\DemoController@readExcel')->name('files.store');
