<?php

namespace Seni\XlsPdf;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

const NUMBER_OF_HEADER_ROWS = 3;

class DemoController extends Controller
{
    // @ToDo Add comments
    // @ToDo Add translations to pdf-template
    // @ToDo Write documentation
    // 1. run 'php artisan vendor:publish'

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('xls-pdf::index');
    }

    /**
     * @param array $data
     * @return mixed
     */
    private function generateAndDownloadPdf(array $data)
    {
        $pdf = PDF::loadView('xls-pdf::pdf-template', $data);
        return $pdf->download($data['fileName'] . '.pdf');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function readExcel(Request $request)
    {
        $uploadedFile = $request->file('xls');
        $fileExtension = $uploadedFile->getClientOriginalExtension();

        if($fileExtension != "xls" && $fileExtension != "xlsx") {
            return redirect()->back();
        }

        $response['fileName'] = str_replace(['.xls', '.xlsx'], '', $uploadedFile->getClientOriginalName());
        $response['data'] = [];

        try{
            $excelDataAsArray = head(Excel::toArray(new Xls(), $uploadedFile));
            $numberOfRows = count($excelDataAsArray) - 1;
            
            for($i = NUMBER_OF_HEADER_ROWS; $i <= $numberOfRows; $i++) {
                $response['data'][$i - NUMBER_OF_HEADER_ROWS] = $this->makeExcelAssociativeArray($excelDataAsArray[1], $excelDataAsArray[$i]);
            }

            $response['totalRecord'] = count($response['data']);
            return $this->generateAndDownloadPdf($response);
            
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Parse data of excel file
     *
     * @param array $headerExcelDataArray
     * @param array $rowOfExcelDataArray
     * @return array
     */
    private function makeExcelAssociativeArray(array $headerExcelDataArray, array $rowOfExcelDataArray): array
    {
        $data['orderData'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, 0, -77);
        $data['orderData']['loading_num'] = preg_replace('/\.(.*)/', '', $data['orderData']['loading_num']);

        $indexOrder = count($data['orderData']);
        $data['sender'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexOrder, -59);

        $indexSender = $indexOrder + count($data['sender']);
        $data['receiver'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexSender, -42);
        $data['receiver']['receiver_address_other'] = explode(',', $data['receiver']['receiver_address_other']);

        foreach ($data['receiver']['receiver_address_other'] as $key => $value) {
            $info = explode(';', $value);
            $data['receiver']['receiver_address_other'][$key] = ['product' => $info[0]];
            $data['receiver']['receiver_address_other'][$key] += ['quantity' => $info[1]];
            $data['receiver']['receiver_address_other'][$key] += ['barcode' => $info[2]];
        }

        $indexReceiver = $indexSender + count($data['receiver']);
        $data['orderDetails'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexReceiver, -32);

        $indexOrderData = $indexReceiver + count($data['orderDetails']);
        $data['senderPayments'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexOrderData, -30);

        $indexSenderPayments = $indexOrderData + count($data['senderPayments']);
        $data['receiverPayments'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexSenderPayments, -27);

        $indexReceiverPayments = $indexSenderPayments + count($data['receiverPayments']);
        $data['thirdPartPayments'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexReceiverPayments, -25);

        $indexThirdPartPayments = $indexReceiverPayments + count($data['thirdPartPayments']);
        $data['paymentDetails'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexThirdPartPayments, -20);

        $indexPaymentDetails = $indexThirdPartPayments + count($data['paymentDetails']);
        $data['packaging'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexPaymentDetails, -11);

        $indexPackaging = $indexPaymentDetails + count($data['packaging']);
        $data['priority'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexPackaging, -9);

        $indexPriority = $indexPackaging + count($data['priority']);
        $data['notification'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexPriority, -8);

        $indexNotification = $indexPriority + count($data['notification']);
        $data['services'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexNotification, -5);

        $indexServices = $indexNotification + count($data['services']);
        $data['other'] = array_splice_and_get_values($headerExcelDataArray, $rowOfExcelDataArray, $indexServices, 0);

        return $data;
    }
    
}