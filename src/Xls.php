<?php

namespace Seni\XlsPdf;

use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class Xls implements ToArray, WithChunkReading
{
    /**
     * @param array $array
     */
    public function array(array $array)
    {
        // TODO: Implement array() method.
    }
    
    /**
     *  Read the spreadsheet in chunks and 
     *  keep the memory usage under control
     */
    public function chunkSize(): int
    {
        return 1000;
    }
}