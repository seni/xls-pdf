<div class="container">
    <div class="row">
        {{-- @ToDo list errors --}}
        <div class="mx-auto col-5">
            <form action="{{route('files.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="file">Upload your excel file</label>

                    <input type="file"
                           id="xls" class="form-control-file" name="xls"
                           accept=".xls,.xlsx" required="required">
                </div>
                <button type="submit" class="btn btn-success"> Submit</button>
            </form>
        </div>
    </div>
</div>