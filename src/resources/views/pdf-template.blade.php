<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>PDF</title>
    </head>
    <body style="font-family: DejaVu Sans; font-size: 20px">
    <style>
        h3, p{
            margin-top: 0;
            margin-bottom: 0;
        }
        .border{
            border-left: 0;
            border-right: 0;
            border-bottom: 3px solid #000;
        }
    </style>
    @foreach($data as $key => $item)
        <table style="width: 100%;">
            <tr>
                <td colspan="2">
                    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($item['orderData']['loading_num'], 'C39', 1, 60)}}" alt="barcode" />
                    <p style="margin: 0">
                        {{ $item['orderData']['loading_num'] }}
                    </p>
                </td>
                <td colspan="2" style="text-align: right">
                    <img src="{{asset('vendor/xls-pdf/logo.png')}}" alt="Econt" width="150px"  style="text-align: right">
                    <p style="margin-bottom: 0;font-size: 20px">
                        <span style="font-size: 25px">{{ $key+1 }}</span>
                        от общо
                        <span style="font-size: 25px">{{ $totalRecord }}</span>
                        пратки
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="border">
                    <h3>ПОДАТЕЛ</h3>
                </td>
                <td class="border">приета:</td>
            </tr>
            <tr>
                <td colspan="3">
                    <p style="margin-bottom: 20px">
                        <b>
                            {{ $item['sender']['sender_city_zip'] ? '('.$item['sender']['sender_city_zip'] . ')' : '' }}
                            {{ $item['sender']['direction_office_from'] }}
                        </b>
                    </p>
                    <p style="margin-bottom: 20px">
                        {{ $item['sender']['sender_name'] }}
                        <br />
                        {{ $item['sender']['sender_city_name'] }}, тел. {{ $item['sender']['sender_phone'] }}
                    </p>
                </td>
                <td>приета:</td>
            </tr>
            <tr>
                <td colspan="3" class="border">
                    <h3>ПОЛУЧАТЕЛ</h3>
                </td>
                <td class="border">зона: <span style="text-align: right">?</span></td>
            </tr>

            <tr>
                <td colspan="4" class="border">
                    <p style="margin-bottom: 20px">
                        <b>
                            {{ $item['receiver']['receiver_city_zip'] ? '('.$item['receiver']['receiver_city_zip'] . ')' : '' }}
                            {{ $item['receiver']['direction_office_to'] }}
                        </b>
                    </p>
                    <p style="margin-bottom: 20px">
                        {{ $item['receiver']['receiver_name'] }}, {{ $item['receiver']['receiver_face'] }}
                        <br />
                        {{ $item['receiver']['receiver_city_name'] }},
                        бл.{{ $item['receiver']['receiver_address_bl'] ?: '' }},
                        вх.{{ $item['receiver']['receiver_address_vh'] ?: '' }},
                        ет.{{ $item['receiver']['receiver_address_et'] ?: '' }},
                        ап.{{ $item['receiver']['receiver_address_ap'] ?: '' }},
                        ул.{{ $item['receiver']['receiver_street'] ?: '' }},
                        тел. {{ $item['receiver']['receiver_phone'] }}
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <p style="margin-bottom: 20px">
                        брой: {{ $item['orderDetails']['shipment_pack_count'] }}
                        тегло: {{ number_format((float)$item['orderDetails']['shipment_pack_weight'], 2, '.', '') }}
                        тар. код: {{ $item['orderDetails']['tariff_code'] ?: '' }}
                        {{ $item['orderDetails']['shipment_description'] }}
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="border">
                    части:
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    Куриерска услуга:
                </td>
            </tr>
            <tr>
                <td colspan="2" class="border">
                    <h3>ПЛАЩАНЕ</h3>
                </td>
                <td colspan="2" class="border">
                    <span style="text-align: right; font-weight: 800;">Общо: събери
                        {{ number_format((float)$item['services']['service_cd_sum'], 2, '.', '') }} лв.
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    Услуги:
                </td>
                <td colspan="3">
                    НП:  {{ number_format((float)$item['services']['service_cd_sum'], 2, '.', '') }} лв. (събери)
                </td>
            </tr>
        </table>
        <footer style="border-top:30px; font-size: 10px; text-align: center">
            Доставка на пратка съгласно общите условия на Еконт Експрес ООД публикувани на http://www.econt.com
        </footer>
        <div style="page-break-before: always;"></div>
    @endforeach
    </body>
</html>