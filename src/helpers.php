<?php

if (! function_exists('array_splice_and_get_values')) {

    /**
     * @param array $arrayKeys - array of columns
     * @param array $arrayValues - array of values
     * @param int|null $offset - from cell "A"
     * @param int|null $length - to cell "M"
     * @return array
     */
    function array_splice_and_get_values(array $arrayKeys, array $arrayValues, ?int $offset, ?int $length): array
    {
        $data = [];
        $data += array_combine(array_values($arrayKeys), array_values($arrayValues));

        return array_splice($data, $offset, $length);
    }
}