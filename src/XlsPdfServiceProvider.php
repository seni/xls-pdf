<?php
namespace Seni\XlsPdf;

use Illuminate\Support\ServiceProvider;

class XlsPdfServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views/', 'xls-pdf');
        $this->publishes([
            __DIR__.'/public/images' => public_path('vendor/xls-pdf'),
        ], 'public');
    }


    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {
        $this->app->make('Seni\XlsPdf\DemoController');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        require_once __DIR__ . '/helpers.php';
    }
}